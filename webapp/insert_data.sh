#!/bin/bash

# Define the data to be inserted

#run this after creating the containers
docker exec -it db psql -U postgres -c "CREATE TABLE stock_prices (date date NOT NULL, open numeric(10, 2) NOT NULL, high numeric(10, 2) NOT NULL, low numeric(10, 2) NOT NULL, close numeric(10, 2) NOT NULL, adj_close numeric(10, 2) NOT NULL, volume bigint NOT NULL);"
#this command creates the table stock_prices with the rows date, open, high, low, close, adj_close, and volume
data=(
    "2022-01-03 100.00 105.00 99.00 100.00 99.00 10000"
    "2022-01-04 101.00 102.00 99.50 100.50 99.50 5000"
    "2022-01-05 102.00 104.00 100.00 101.00 100.00 6000"
)

# Loop through the data array and insert each row into the table
for row in "${data[@]}"; do
    date=$(echo $row | awk '{print $1}')
    open=$(echo $row | awk '{print $2}')
    high=$(echo $row | awk '{print $3}')
    low=$(echo $row | awk '{print $4}')
    close=$(echo $row | awk '{print $5}')
    adj_close=$(echo $row | awk '{print $6}')
    volume=$(echo $row | awk '{print $7}')
    docker exec -it postgres psql -U postgres -c "INSERT INTO stock_prices (date, open, high, low, close, adj_close, volume)
        VALUES ('$date', $open, $high, $low, $close, $adj_close, $volume);"
done
