import psycopg2
from flask import Flask, render_template


app = Flask(__name__)


@app.route("/")
def index():
    conn = psycopg2.connect(
        host="db",
        database="postgres",
        user="postgres",
        password="password"
    )
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS test (id serial PRIMARY KEY, num integer, data varchar);")
    cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (100, "abcdef"))
    cur.execute("SELECT * FROM test")
    rows = cur.fetchall()
    return render_template("./index.html", rows=rows)

if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5001,debug=True)
